#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils.h"

int VERBOSE = 0;

const char *get_all_digits() { return "0123456789abcdefghijklmnopqrstuvwxyz"; }
const size_t ALL_DIGIT_COUNT = 36;

void arithmatoy_free(char *number) { free(number); }

char *arithmatoy_add(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "add: entering function\n");
  }

  // Fill the function, the goal is to compute lhs + rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);
  size_t lengthLhs = strlen(lhs);
  size_t LengthRhs = strlen(rhs);

  if (lengthLhs < LengthRhs) {
    // Swap the operands if the first one is shorter
    const char *tmp = lhs;
    lhs = rhs;
    rhs = tmp;

    const size_t lengthTmp = lengthLhs;
    lengthLhs = LengthRhs;
    LengthRhs = lengthTmp;
  }

  char *result = calloc(lengthLhs + 1, sizeof(char)); // +1 for the final \0
  if (result == NULL) {
    debug_abort("add: calloc failed\n");
  }

  unsigned int lhs_digit, rhs_digit, sum;
  unsigned int carry = 0;
  for (size_t i = 0; i < lengthLhs; ++i) {
    lhs_digit = get_digit_value(lhs[lengthLhs - i - 1]);
    rhs_digit = i < LengthRhs ? get_digit_value(rhs[LengthRhs - i - 1]) : 0;
    if (VERBOSE) {
      fprintf(
          stderr,
          "add: digit %c digit %c carry %u\n",
          to_digit(lhs_digit),
          to_digit(rhs_digit),
          carry
      );
    }

    sum = lhs_digit + rhs_digit + carry;
    carry = sum / base;
    sum = sum % base;
    if (VERBOSE) {
      fprintf(stderr, "add: result: digit %c carry %u\n", to_digit(sum), carry);
    }

    result[lengthLhs - i - 1] = to_digit(sum);
  }

  char *result_final;
  if (carry != 0) {
    if (VERBOSE) {
      fprintf(stderr, "add: final carry %u\n", carry);
    }

    // If the last carry is non-zero, the result is one digit longer than the
    // longest operand, so we need to allocate a new string and prepend the
    // carry
    result_final = calloc(lengthLhs + 2, sizeof(char));
    result_final[0] = to_digit(carry);
    strcpy(result_final + 1, result);
    arithmatoy_free(result);
  } else {
    result_final = result;
  }

  return result_final;
}

char *arithmatoy_sub(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "sub: entering function\n");
  }

  // Fill the function, the goal is to compute lhs - rhs (assuming lhs > rhs)
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);
  const size_t lengthLhs = strlen(lhs);
  const size_t lengthRhs = strlen(rhs);

  if (lengthLhs < lengthRhs) {
    return NULL;
    //Si a < b alors res < 0 donc erreur
  }

  char *result = calloc(lengthLhs + 1, sizeof(char));
  if (result == NULL) {
    debug_abort("sub: calloc failed\n");
  }

  unsigned int carry = 0;
  unsigned int lhs_digit, rhs_digit;
  int diff;
  for (size_t i = 0; i < lengthLhs; ++i) {
    lhs_digit = get_digit_value(lhs[lengthLhs - i - 1]);
    rhs_digit = i < lengthRhs ? get_digit_value(rhs[lengthRhs - i - 1]) : 0;
    if (VERBOSE) {
      fprintf(
          stderr,
          "sub: digit %c digit %c carry %u\n",
          to_digit(lhs_digit),
          to_digit(rhs_digit),
          carry
      );
    }

    diff = lhs_digit - rhs_digit - carry;
    if (diff < 0) {
      carry = 1;
      diff += base;
    } else {
      carry = 0;
    }
    if (VERBOSE) {
      fprintf(
          stderr, "sub: result: digit %c carry %u\n", to_digit(diff), carry
      );
    }

    result[lengthLhs - i - 1] = to_digit(diff);
  }

  if (carry != 0) {
    arithmatoy_free(result);
    return NULL;
  }

  char *result_old = result;
  result = (char *)drop_leading_zeros(result);
  char *result_final = calloc(strlen(result) + 1, sizeof(char));
  strcpy(result_final, result);
  arithmatoy_free(result_old);

  return result_final;

}

char *arithmatoy_mul(unsigned int base, const char *lhs, const char *rhs) {
  if (VERBOSE) {
    fprintf(stderr, "mul: entering function\n");
  }

  // Fill the function, the goal is to compute lhs * rhs
  // You should allocate a new char* large enough to store the result as a
  // string Implement the algorithm Return the result

  lhs = drop_leading_zeros(lhs);
  rhs = drop_leading_zeros(rhs);
  size_t lengthLhs = strlen(lhs);
  size_t lengthRhs = strlen(rhs);

  if (lengthLhs < lengthRhs) {
    // Swap the operands if the first one is shorter
    const char *tmp = lhs;
    lhs = rhs;
    rhs = tmp;
    const size_t lengthTmp = lengthLhs;
    lengthLhs = lengthRhs;
    lengthRhs = lengthTmp;
  }

  // The result size can be at most the sum of the lengths of the inputs
  const size_t result_len = lengthLhs + lengthRhs;
  char *result = calloc(result_len + 1, sizeof(char)); // +1 for the final \0
  if (result == NULL) {
    debug_abort("mul: calloc failed\n");
  }
  char *partial_result = calloc(result_len + 1, sizeof(char));
  if (partial_result == NULL) {
    arithmatoy_free(result);
    debug_abort("mul: calloc failed\n");
  }

  // Set the result to zero
  result[0] = '0';

  // Multiply each digit of rhs by each digit of lhs
  unsigned int lhs_digit, rhs_digit, product, carry;
  for (size_t i = 0; i < lengthRhs; ++i) {
    // Reset the partial result to all zeros
    memset(partial_result, '0', result_len);

    carry = 0;

    rhs_digit = get_digit_value(rhs[lengthRhs - i - 1]);
    if (VERBOSE) {
      fprintf(stderr, "mul: digit %c number %s\n", to_digit(rhs_digit), lhs);
    }

    // Multiply the current digit of rhs by each digit of lhs
    for (size_t j = 0; j < lengthLhs; ++j) {
      lhs_digit = get_digit_value(lhs[lengthLhs - j - 1]);
      if (VERBOSE) {
        fprintf(
            stderr,
            "mul: digit %c digit %c carry %u\n",
            to_digit(rhs_digit),
            to_digit(lhs_digit),
            carry
        );
      }

      product = lhs_digit * rhs_digit + carry;
      carry = product / base;
      product = product % base;
      if (VERBOSE) {
        fprintf(
            stderr, "mul: result: digit %c carry %u\n", to_digit(product), carry
        );
      }

      partial_result[result_len - j - i - 1] = to_digit(product);
    }

    if (carry != 0) {
      if (VERBOSE) {
        fprintf(stderr, "mul: final carry %c\n", to_digit(carry));
      }

      // If the last carry is non-zero, add a digit to the result
      partial_result[result_len - lengthLhs - i - 1] = to_digit(carry);
    }

    if (VERBOSE) {
      fprintf(
          stderr,
          "mul: add %s + %s\n",
          drop_leading_zeros(result),
          // Drop leading zeros of the printed result, unless it is 0
          // In this case, print a string of zeros to match the expected output
          partial_result + result_len - lengthLhs - i - (carry != 0)
      );
    }

    // Add the partial result to the final result
    char *tmp = arithmatoy_add(base, result, partial_result);
    if (tmp == NULL) {
      debug_abort("mul: arithmatoy_add failed\n");
    }

    arithmatoy_free(result);
    result = tmp;

    if (VERBOSE) {
      fprintf(stderr, "mul: result: %s\n", result);
    }
  }

  arithmatoy_free(partial_result);

  return result;

}

// Here are some utility functions that might be helpful to implement add, sub
// and mul:

unsigned int get_digit_value(char digit) {
  // Convert a digit from get_all_digits() to its integer value
  if (digit >= '0' && digit <= '9') {
    return digit - '0';
  }
  if (digit >= 'a' && digit <= 'z') {
    return 10 + (digit - 'a');
  }
  return -1;
}

char to_digit(unsigned int value) {
  // Convert an integer value to a digit from get_all_digits()
  if (value >= ALL_DIGIT_COUNT) {
    debug_abort("Invalid value for to_digit()");
    return 0;
  }
  return get_all_digits()[value];
}

char *reverse(char *str) {
  // Reverse a string in place, return the pointer for convenience
  // Might be helpful if you fill your char* buffer from left to right
  const size_t length = strlen(str);
  const size_t bound = length / 2;
  for (size_t i = 0; i < bound; ++i) {
    char tmp = str[i];
    const size_t mirror = length - i - 1;
    str[i] = str[mirror];
    str[mirror] = tmp;
  }
  return str;
}

const char *drop_leading_zeros(const char *number) {
  // If the number has leading zeros, return a pointer past these zeros
  // Might be helpful to avoid computing a result with leading zeros
  if (*number == '\0') {
    return number;
  }
  while (*number == '0') {
    ++number;
  }
  if (*number == '\0') {
    --number;
  }
  return number;
}

void debug_abort(const char *debug_msg) {
  // Print a message and exit
  fprintf(stderr, debug_msg);
  exit(EXIT_FAILURE);
}
