import math

def nombre_entier(n: int) -> str:
    return "0" if n == 0 else ("S" * n) + "0"

def S(n: str) -> str:
    return "S" + n


def addition(a: str, b: str) -> str:
    if a == "0":
        return b
    if b == "0":
        return a
    return S(addition(a[1:], b))


def multiplication(a: str, b: str) -> str:
    if a == "0" or b == "0":
        return "0"
    else:
        return addition(b, multiplication(a[:-1], b))

def facto_ite(n: int) -> int:
    if n < 0:
        raise ValueError("dans la factorisation l'entier en paramètre ne peut être négatif")
    facto = 1
    for i in range(1, n + 1):
        facto *= i
    return facto


def facto_rec(n: int) -> int:
    if n < 0:
        raise ValueError("dans la factorisation l'entier en paramètre ne peut être négatif")
    if n == 1 or n == 0:
        return 1
    else:
        return n * facto_rec(n - 1)


def fibo_rec(n: int) -> int:
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fibo_rec(n-1) + fibo_rec(n-2)


def fibo_ite(n: int) -> int:
    a, b = 0, 1
    for i in range(n):
        a, b = b, a+b
    return a


def golden_phi(n: int) -> int:
    phi = (1 + math.sqrt(5)) / 2
    return (phi ** n - (1 - phi) ** n) / math.sqrt(5)

def sqrt5(n: int) -> int:
    return 2 * golden_phi(n+1) -1


def pow(a: float, n: int) -> float:
    if n == 0:
        return 1
    elif n % 2 == 0:
        return pow(a * a, n // 2)
    else:
        return a * pow(a, n-1)
